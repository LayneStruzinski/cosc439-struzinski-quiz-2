#include<stdio.h>
#include<sys/wait.h>
#include <unistd.h>
#include <wait.h>
#include <stdlib.h>

#define DIM 9
int values[DIM] = { 1, 60, 9, 18, 91, 7, 26, 75, 6};

void print_max(){
	int max = 0;
	for(int i = 0; i < DIM; i++) {
		if(max < values[i]) {
			max = values[i];
		}
	}
	printf("Maximum is %d\n", max);
}

void print_avg() {
	int sum = 0;
	for(int i = 0; i < DIM; i++) {
		sum += values[i];
	}
	printf("Average is %f\n", (float) (sum) / DIM);
}

void print_values() {
	printf("Values: ");
	for(int i = 0; i < DIM; i++) {
		printf("%d ", values[i]);
	}
	printf("\n");
} 

int main() {

	pid_t pidA;
	pid_t pidB;
	
	pidA = fork();
	
	if(pidA == 0) {
		print_avg();
		exit(0);
	}
	else {
		pidB = fork();
		
		if(pidB == 0) {
			print_max();
			exit(0);
		}
		else {
			
			print_values();
			wait(NULL);	
		}
	}
	return 0;
}
